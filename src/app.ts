import mongoose from "mongoose";
import { ApolloServer } from "apollo-server"
import "dotenv/config"
import { schema } from "./graphql/schema";

const server = new ApolloServer({ schema })

mongoose.connect(process.env.dbconn!);

server.listen(4000, () => {
    console.log("started");
})
