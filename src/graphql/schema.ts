import { schemaComposer } from "graphql-compose";
import { composeMongoose } from "graphql-compose-mongoose";
import { OrderImportanceModel } from "../models/OrderImportanceSchema";
import { OrderModel } from "../models/OrderSchema";
import { ProductionDepartmentModel } from "../models/ProductionDepartmentSchema";
import { RoleModel } from "../models/RoleSchema";
import { TeamModel } from "../models/TeamSchema";
import { UserModel } from "../models/UserSchema";

const UserSchema = composeMongoose(UserModel);
const RoleSchema = composeMongoose(RoleModel);
const TeamSchema = composeMongoose(TeamModel);
const OrderImportanceSchema = composeMongoose(OrderImportanceModel);
const OrderSchema = composeMongoose(OrderModel);
const ProductionDepartmentSchema = composeMongoose(ProductionDepartmentModel);

schemaComposer.Query.addFields({
  userById: UserSchema.mongooseResolvers.findById(),
  userOne: UserSchema.mongooseResolvers.findOne(),
  userMany: UserSchema.mongooseResolvers.findMany(),

  roleById: RoleSchema.mongooseResolvers.findById(),
  roleOne: RoleSchema.mongooseResolvers.findOne(),
  roleMany: RoleSchema.mongooseResolvers.findMany(),

  productionDepartmentById:
    ProductionDepartmentSchema.mongooseResolvers.findById(),
  productionDepartmentOne:
    ProductionDepartmentSchema.mongooseResolvers.findOne(),
  productionDepartmentMany:
    ProductionDepartmentSchema.mongooseResolvers.findMany(),

  teamById: TeamSchema.mongooseResolvers.findById(),
  teamOne: TeamSchema.mongooseResolvers.findOne(),
  teamMany: TeamSchema.mongooseResolvers.findMany(),

  orderImportanceById: OrderImportanceSchema.mongooseResolvers.findById(),
  orderImportanceOne: OrderImportanceSchema.mongooseResolvers.findOne(),
  orderImportanceMany: OrderImportanceSchema.mongooseResolvers.findMany(),

  orderById: OrderSchema.mongooseResolvers.findById(),
  orderOne: OrderSchema.mongooseResolvers.findOne(),
  orderMany: OrderSchema.mongooseResolvers.findMany(),
});

schemaComposer.Mutation.addFields({
  addUser: UserSchema.mongooseResolvers.createOne(),
  addRole: RoleSchema.mongooseResolvers.createOne(),
  addProductionDepartment:
    ProductionDepartmentSchema.mongooseResolvers.createOne(),

  addTeam: TeamSchema.mongooseResolvers.createOne(),
  updateTeamById: TeamSchema.mongooseResolvers.updateById(),
  updateTeamOne: TeamSchema.mongooseResolvers.updateOne(),
  updateTeamMany: TeamSchema.mongooseResolvers.updateMany(),
  removeTeamById: TeamSchema.mongooseResolvers.removeById(),
  removeTeamOne: TeamSchema.mongooseResolvers.removeOne(),

  addOrderImportance: OrderImportanceSchema.mongooseResolvers.createOne(),

  addOrder: OrderSchema.mongooseResolvers.createOne(),
  updateOrderOne: OrderSchema.mongooseResolvers.updateOne(),
});

UserSchema.addRelation("role", {
  resolver: () => RoleSchema.mongooseResolvers.findById(),
  prepareArgs: {
    _id: (source) => source.role,
  },
  projection: { role: 1 },
});

TeamSchema.addRelation("productionDepartment", {
  resolver: () => ProductionDepartmentSchema.mongooseResolvers.findById(),
  prepareArgs: {
    _id: (source) => source.productionDepartment,
  },
  projection: { productionDepartment: 1 },
});

TeamSchema.addRelation("master", {
  resolver: () => UserSchema.mongooseResolvers.findById(),
  prepareArgs: {
    _id: (source) => source.master,
  },
  projection: { master: 1 },
});

TeamSchema.addRelation("workers", {
  resolver: () => UserSchema.mongooseResolvers.findByIds(),
  prepareArgs: {
    _ids: (source) => source.workers,
  },
  projection: { workers: 1 },
});

OrderSchema.addRelation("team", {
  resolver: () => TeamSchema.mongooseResolvers.findById(),
  prepareArgs: {
    _id: (source) => source.team,
  },
  projection: { team: 1 },
});

OrderSchema.addRelation("orderType", {
  resolver: () => OrderImportanceSchema.mongooseResolvers.findById(),
  prepareArgs: {
    _id: (source) => source.orderType,
  },
  projection: { orderType: 1 },
});

export const schema = schemaComposer.buildSchema();
