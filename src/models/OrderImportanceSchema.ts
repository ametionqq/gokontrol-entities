import mongoose from "mongoose";
import * as Types from "./types/types";

const OrderImportanceSchema: Types.OrdersImportanceSchema = new mongoose.Schema(
  {
    importanceLevel: {
      type: String,
      required: true,
      unique: true,
    },
    importanceColor: {
      type: String,
      required: true,
      unique: true,
    },
  }
);

export const OrderImportanceModel = mongoose.model<
  Types.OrdersImportanceDocument,
  Types.OrdersImportanceModel
>("OrdersImportance", OrderImportanceSchema);
