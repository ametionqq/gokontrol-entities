import mongoose from "mongoose";
import * as Types from "./types/types";

const OrderSchema: Types.OrdersSchema = new mongoose.Schema({
  orderDescription: {
    type: String,
    required: true,
  },
  expectedReceiptDate: {
    type: Date,
  },
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Teams",
  },
  orderType: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "OrdersImportance",
  },
});

export const OrderModel = mongoose.model<
  Types.OrdersDocument,
  Types.OrdersModel
>("Orders", OrderSchema);
