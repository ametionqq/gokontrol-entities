import mongoose from "mongoose";
import * as Types from "./types/types";

const ProductionDepartmentSchema: Types.ProductionDepartmentsSchema =
  new mongoose.Schema({
    departmentName: {
      type: String,
      unique: true,
      required: true,
    },
    departmentDescription: {
      type: String,
    },
  });

export const ProductionDepartmentModel = mongoose.model<
  Types.ProductionDepartmentsDocument,
  Types.ProductionDepartmentsModel
>("ProductionDepartments", ProductionDepartmentSchema);
