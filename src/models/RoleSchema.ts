import mongoose from "mongoose";
import * as Types from "./types/types";

const RoleSchema: Types.RolesSchema = new mongoose.Schema({
  role: {
    type: String,
    required: true,
    unique: true,
  },
  abbreviation: {
    type: String,
    required: true,
    unique: true,
  },
});

export const RoleModel = mongoose.model<Types.RolesDocument, Types.RolesModel>(
  "Roles",
  RoleSchema
);
