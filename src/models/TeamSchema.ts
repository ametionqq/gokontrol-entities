import mongoose from "mongoose";
import * as Types from "./types/types";

const TeamSchema: Types.TeamsSchema = new mongoose.Schema({
  teamName: {
    type: String,
    required: true,
    unique: true,
  },
  master: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Users",
  },
  workers: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
  ],
  productionDepartment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Departments",
  },
});

export const TeamModel = mongoose.model<Types.TeamsDocument, Types.TeamsModel>(
  "Teams",
  TeamSchema
);
