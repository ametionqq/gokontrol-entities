import mongoose from "mongoose";
import * as Types from "./types/types";

const UserSchema: Types.UsersSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  secondName: {
    type: String,
    required: true,
  },
  login: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Roles",
    required: true,
  },
});

export const UserModel = mongoose.model<Types.UsersDocument, Types.UsersModel>(
  "Users",
  UserSchema
);
