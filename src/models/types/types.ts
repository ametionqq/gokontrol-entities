/* tslint:disable */
/* eslint-disable */

//@ts-nocheck

// ######################################## THIS FILE WAS GENERATED BY MONGOOSE-TSGEN ######################################## //

// NOTE: ANY CHANGES MADE WILL BE OVERWRITTEN ON SUBSEQUENT EXECUTIONS OF MONGOOSE-TSGEN.

import mongoose from "mongoose";
src / models / OrderImportanceSchema.ts;
src / models / OrderSchema.ts;
src / models / ProductionDepartmentSchema.ts;
src / models / RoleSchema.ts;
src / models / TeamSchema.ts;
src / models / UserSchema.ts;

/**
 * Lean version of OrdersImportanceDocument
 *
 * This has all Mongoose getters & functions removed. This type will be returned from `OrdersImportanceDocument.toObject()`. To avoid conflicts with model names, use the type alias `OrdersImportanceObject`.
 * ```
 * const ordersimportanceObject = ordersimportance.toObject();
 * ```
 */
export type OrdersImportance = {
  importanceLevel: string;
  importanceColor: string;
  _id: mongoose.Types.ObjectId;
};

/**
 * Lean version of OrdersImportanceDocument (type alias of `OrdersImportance`)
 *
 * Use this type alias to avoid conflicts with model names:
 * ```
 * import { OrdersImportance } from "../models"
 * import { OrdersImportanceObject } from "../interfaces/mongoose.gen.ts"
 *
 * const ordersimportanceObject: OrdersImportanceObject = ordersimportance.toObject();
 * ```
 */
export type OrdersImportanceObject = OrdersImportance;

/**
 * Mongoose Query type
 *
 * This type is returned from query functions. For most use cases, you should not need to use this type explicitly.
 */
export type OrdersImportanceQuery = mongoose.Query<
  any,
  OrdersImportanceDocument,
  OrdersImportanceQueries
> &
  OrdersImportanceQueries;

/**
 * Mongoose Query helper types
 *
 * This type represents `OrdersImportanceSchema.query`. For most use cases, you should not need to use this type explicitly.
 */
export type OrdersImportanceQueries = {};

export type OrdersImportanceMethods = {};

export type OrdersImportanceStatics = {};

/**
 * Mongoose Model type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const OrdersImportance = mongoose.model<OrdersImportanceDocument, OrdersImportanceModel>("OrdersImportance", OrdersImportanceSchema);
 * ```
 */
export type OrdersImportanceModel = mongoose.Model<
  OrdersImportanceDocument,
  OrdersImportanceQueries
> &
  OrdersImportanceStatics;

/**
 * Mongoose Schema type
 *
 * Assign this type to new OrdersImportance schema instances:
 * ```
 * const OrdersImportanceSchema: OrdersImportanceSchema = new mongoose.Schema({ ... })
 * ```
 */
export type OrdersImportanceSchema = mongoose.Schema<
  OrdersImportanceDocument,
  OrdersImportanceModel,
  OrdersImportanceMethods,
  OrdersImportanceQueries
>;

/**
 * Mongoose Document type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const OrdersImportance = mongoose.model<OrdersImportanceDocument, OrdersImportanceModel>("OrdersImportance", OrdersImportanceSchema);
 * ```
 */
export type OrdersImportanceDocument = mongoose.Document<
  mongoose.Types.ObjectId,
  OrdersImportanceQueries
> &
  OrdersImportanceMethods & {
    importanceLevel: string;
    importanceColor: string;
    _id: mongoose.Types.ObjectId;
  };

/**
 * Lean version of OrdersDocument
 *
 * This has all Mongoose getters & functions removed. This type will be returned from `OrdersDocument.toObject()`. To avoid conflicts with model names, use the type alias `OrdersObject`.
 * ```
 * const ordersObject = orders.toObject();
 * ```
 */
export type Orders = {
  orderDescription: string;
  expectedReceiptDate?: Date;
  team?: Teams["_id"] | Teams;
  orderType: OrdersImportance["_id"] | OrdersImportance;
  _id: mongoose.Types.ObjectId;
};

/**
 * Lean version of OrdersDocument (type alias of `Orders`)
 *
 * Use this type alias to avoid conflicts with model names:
 * ```
 * import { Orders } from "../models"
 * import { OrdersObject } from "../interfaces/mongoose.gen.ts"
 *
 * const ordersObject: OrdersObject = orders.toObject();
 * ```
 */
export type OrdersObject = Orders;

/**
 * Mongoose Query type
 *
 * This type is returned from query functions. For most use cases, you should not need to use this type explicitly.
 */
export type OrdersQuery = mongoose.Query<any, OrdersDocument, OrdersQueries> &
  OrdersQueries;

/**
 * Mongoose Query helper types
 *
 * This type represents `OrdersSchema.query`. For most use cases, you should not need to use this type explicitly.
 */
export type OrdersQueries = {};

export type OrdersMethods = {};

export type OrdersStatics = {};

/**
 * Mongoose Model type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Orders = mongoose.model<OrdersDocument, OrdersModel>("Orders", OrdersSchema);
 * ```
 */
export type OrdersModel = mongoose.Model<OrdersDocument, OrdersQueries> &
  OrdersStatics;

/**
 * Mongoose Schema type
 *
 * Assign this type to new Orders schema instances:
 * ```
 * const OrdersSchema: OrdersSchema = new mongoose.Schema({ ... })
 * ```
 */
export type OrdersSchema = mongoose.Schema<
  OrdersDocument,
  OrdersModel,
  OrdersMethods,
  OrdersQueries
>;

/**
 * Mongoose Document type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Orders = mongoose.model<OrdersDocument, OrdersModel>("Orders", OrdersSchema);
 * ```
 */
export type OrdersDocument = mongoose.Document<
  mongoose.Types.ObjectId,
  OrdersQueries
> &
  OrdersMethods & {
    orderDescription: string;
    expectedReceiptDate?: Date;
    team?: TeamsDocument["_id"] | TeamsDocument;
    orderType: OrdersImportanceDocument["_id"] | OrdersImportanceDocument;
    _id: mongoose.Types.ObjectId;
  };

/**
 * Lean version of ProductionDepartmentsDocument
 *
 * This has all Mongoose getters & functions removed. This type will be returned from `ProductionDepartmentsDocument.toObject()`. To avoid conflicts with model names, use the type alias `ProductionDepartmentsObject`.
 * ```
 * const productiondepartmentsObject = productiondepartments.toObject();
 * ```
 */
export type ProductionDepartments = {
  departmentName: string;
  departmentDescription?: string;
  _id: mongoose.Types.ObjectId;
};

/**
 * Lean version of ProductionDepartmentsDocument (type alias of `ProductionDepartments`)
 *
 * Use this type alias to avoid conflicts with model names:
 * ```
 * import { ProductionDepartments } from "../models"
 * import { ProductionDepartmentsObject } from "../interfaces/mongoose.gen.ts"
 *
 * const productiondepartmentsObject: ProductionDepartmentsObject = productiondepartments.toObject();
 * ```
 */
export type ProductionDepartmentsObject = ProductionDepartments;

/**
 * Mongoose Query type
 *
 * This type is returned from query functions. For most use cases, you should not need to use this type explicitly.
 */
export type ProductionDepartmentsQuery = mongoose.Query<
  any,
  ProductionDepartmentsDocument,
  ProductionDepartmentsQueries
> &
  ProductionDepartmentsQueries;

/**
 * Mongoose Query helper types
 *
 * This type represents `ProductionDepartmentsSchema.query`. For most use cases, you should not need to use this type explicitly.
 */
export type ProductionDepartmentsQueries = {};

export type ProductionDepartmentsMethods = {};

export type ProductionDepartmentsStatics = {};

/**
 * Mongoose Model type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const ProductionDepartments = mongoose.model<ProductionDepartmentsDocument, ProductionDepartmentsModel>("ProductionDepartments", ProductionDepartmentsSchema);
 * ```
 */
export type ProductionDepartmentsModel = mongoose.Model<
  ProductionDepartmentsDocument,
  ProductionDepartmentsQueries
> &
  ProductionDepartmentsStatics;

/**
 * Mongoose Schema type
 *
 * Assign this type to new ProductionDepartments schema instances:
 * ```
 * const ProductionDepartmentsSchema: ProductionDepartmentsSchema = new mongoose.Schema({ ... })
 * ```
 */
export type ProductionDepartmentsSchema = mongoose.Schema<
  ProductionDepartmentsDocument,
  ProductionDepartmentsModel,
  ProductionDepartmentsMethods,
  ProductionDepartmentsQueries
>;

/**
 * Mongoose Document type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const ProductionDepartments = mongoose.model<ProductionDepartmentsDocument, ProductionDepartmentsModel>("ProductionDepartments", ProductionDepartmentsSchema);
 * ```
 */
export type ProductionDepartmentsDocument = mongoose.Document<
  mongoose.Types.ObjectId,
  ProductionDepartmentsQueries
> &
  ProductionDepartmentsMethods & {
    departmentName: string;
    departmentDescription?: string;
    _id: mongoose.Types.ObjectId;
  };

/**
 * Lean version of RolesDocument
 *
 * This has all Mongoose getters & functions removed. This type will be returned from `RolesDocument.toObject()`. To avoid conflicts with model names, use the type alias `RolesObject`.
 * ```
 * const rolesObject = roles.toObject();
 * ```
 */
export type Roles = {
  role: string;
  abbreviation: string;
  _id: mongoose.Types.ObjectId;
};

/**
 * Lean version of RolesDocument (type alias of `Roles`)
 *
 * Use this type alias to avoid conflicts with model names:
 * ```
 * import { Roles } from "../models"
 * import { RolesObject } from "../interfaces/mongoose.gen.ts"
 *
 * const rolesObject: RolesObject = roles.toObject();
 * ```
 */
export type RolesObject = Roles;

/**
 * Mongoose Query type
 *
 * This type is returned from query functions. For most use cases, you should not need to use this type explicitly.
 */
export type RolesQuery = mongoose.Query<any, RolesDocument, RolesQueries> &
  RolesQueries;

/**
 * Mongoose Query helper types
 *
 * This type represents `RolesSchema.query`. For most use cases, you should not need to use this type explicitly.
 */
export type RolesQueries = {};

export type RolesMethods = {};

export type RolesStatics = {};

/**
 * Mongoose Model type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Roles = mongoose.model<RolesDocument, RolesModel>("Roles", RolesSchema);
 * ```
 */
export type RolesModel = mongoose.Model<RolesDocument, RolesQueries> &
  RolesStatics;

/**
 * Mongoose Schema type
 *
 * Assign this type to new Roles schema instances:
 * ```
 * const RolesSchema: RolesSchema = new mongoose.Schema({ ... })
 * ```
 */
export type RolesSchema = mongoose.Schema<
  RolesDocument,
  RolesModel,
  RolesMethods,
  RolesQueries
>;

/**
 * Mongoose Document type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Roles = mongoose.model<RolesDocument, RolesModel>("Roles", RolesSchema);
 * ```
 */
export type RolesDocument = mongoose.Document<
  mongoose.Types.ObjectId,
  RolesQueries
> &
  RolesMethods & {
    role: string;
    abbreviation: string;
    _id: mongoose.Types.ObjectId;
  };

/**
 * Lean version of TeamsDocument
 *
 * This has all Mongoose getters & functions removed. This type will be returned from `TeamsDocument.toObject()`. To avoid conflicts with model names, use the type alias `TeamsObject`.
 * ```
 * const teamsObject = teams.toObject();
 * ```
 */
export type Teams = {
  teamName: string;
  master?: Users["_id"] | Users;
  workers: (Users["_id"] | Users)[];
  productionDepartment?: Departments["_id"] | Departments;
  _id: mongoose.Types.ObjectId;
};

/**
 * Lean version of TeamsDocument (type alias of `Teams`)
 *
 * Use this type alias to avoid conflicts with model names:
 * ```
 * import { Teams } from "../models"
 * import { TeamsObject } from "../interfaces/mongoose.gen.ts"
 *
 * const teamsObject: TeamsObject = teams.toObject();
 * ```
 */
export type TeamsObject = Teams;

/**
 * Mongoose Query type
 *
 * This type is returned from query functions. For most use cases, you should not need to use this type explicitly.
 */
export type TeamsQuery = mongoose.Query<any, TeamsDocument, TeamsQueries> &
  TeamsQueries;

/**
 * Mongoose Query helper types
 *
 * This type represents `TeamsSchema.query`. For most use cases, you should not need to use this type explicitly.
 */
export type TeamsQueries = {};

export type TeamsMethods = {};

export type TeamsStatics = {};

/**
 * Mongoose Model type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Teams = mongoose.model<TeamsDocument, TeamsModel>("Teams", TeamsSchema);
 * ```
 */
export type TeamsModel = mongoose.Model<TeamsDocument, TeamsQueries> &
  TeamsStatics;

/**
 * Mongoose Schema type
 *
 * Assign this type to new Teams schema instances:
 * ```
 * const TeamsSchema: TeamsSchema = new mongoose.Schema({ ... })
 * ```
 */
export type TeamsSchema = mongoose.Schema<
  TeamsDocument,
  TeamsModel,
  TeamsMethods,
  TeamsQueries
>;

/**
 * Mongoose Document type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Teams = mongoose.model<TeamsDocument, TeamsModel>("Teams", TeamsSchema);
 * ```
 */
export type TeamsDocument = mongoose.Document<
  mongoose.Types.ObjectId,
  TeamsQueries
> &
  TeamsMethods & {
    teamName: string;
    master?: UsersDocument["_id"] | UsersDocument;
    workers: mongoose.Types.Array<UsersDocument["_id"] | UsersDocument>;
    productionDepartment?: DepartmentsDocument["_id"] | DepartmentsDocument;
    _id: mongoose.Types.ObjectId;
  };

/**
 * Lean version of UsersDocument
 *
 * This has all Mongoose getters & functions removed. This type will be returned from `UsersDocument.toObject()`. To avoid conflicts with model names, use the type alias `UsersObject`.
 * ```
 * const usersObject = users.toObject();
 * ```
 */
export type Users = {
  firstName: string;
  secondName: string;
  login: string;
  password: string;
  role: Roles["_id"] | Roles;
  _id: mongoose.Types.ObjectId;
};

/**
 * Lean version of UsersDocument (type alias of `Users`)
 *
 * Use this type alias to avoid conflicts with model names:
 * ```
 * import { Users } from "../models"
 * import { UsersObject } from "../interfaces/mongoose.gen.ts"
 *
 * const usersObject: UsersObject = users.toObject();
 * ```
 */
export type UsersObject = Users;

/**
 * Mongoose Query type
 *
 * This type is returned from query functions. For most use cases, you should not need to use this type explicitly.
 */
export type UsersQuery = mongoose.Query<any, UsersDocument, UsersQueries> &
  UsersQueries;

/**
 * Mongoose Query helper types
 *
 * This type represents `UsersSchema.query`. For most use cases, you should not need to use this type explicitly.
 */
export type UsersQueries = {};

export type UsersMethods = {};

export type UsersStatics = {};

/**
 * Mongoose Model type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Users = mongoose.model<UsersDocument, UsersModel>("Users", UsersSchema);
 * ```
 */
export type UsersModel = mongoose.Model<UsersDocument, UsersQueries> &
  UsersStatics;

/**
 * Mongoose Schema type
 *
 * Assign this type to new Users schema instances:
 * ```
 * const UsersSchema: UsersSchema = new mongoose.Schema({ ... })
 * ```
 */
export type UsersSchema = mongoose.Schema<
  UsersDocument,
  UsersModel,
  UsersMethods,
  UsersQueries
>;

/**
 * Mongoose Document type
 *
 * Pass this type to the Mongoose Model constructor:
 * ```
 * const Users = mongoose.model<UsersDocument, UsersModel>("Users", UsersSchema);
 * ```
 */
export type UsersDocument = mongoose.Document<
  mongoose.Types.ObjectId,
  UsersQueries
> &
  UsersMethods & {
    firstName: string;
    secondName: string;
    login: string;
    password: string;
    role: RolesDocument["_id"] | RolesDocument;
    _id: mongoose.Types.ObjectId;
  };

/**
 * Check if a property on a document is populated:
 * ```
 * import { IsPopulated } from "../interfaces/mongoose.gen.ts"
 *
 * if (IsPopulated<UserDocument["bestFriend"]>) { ... }
 * ```
 */
export function IsPopulated<T>(doc: T | mongoose.Types.ObjectId): doc is T {
  return doc instanceof mongoose.Document;
}

/**
 * Helper type used by `PopulatedDocument`. Returns the parent property of a string
 * representing a nested property (i.e. `friend.user` -> `friend`)
 */
type ParentProperty<T> = T extends `${infer P}.${string}` ? P : never;

/**
 * Helper type used by `PopulatedDocument`. Returns the child property of a string
 * representing a nested property (i.e. `friend.user` -> `user`).
 */
type ChildProperty<T> = T extends `${string}.${infer C}` ? C : never;

/**
 * Helper type used by `PopulatedDocument`. Removes the `ObjectId` from the general union type generated
 * for ref documents (i.e. `mongoose.Types.ObjectId | UserDocument` -> `UserDocument`)
 */
type PopulatedProperty<Root, T extends keyof Root> = Omit<Root, T> & {
  [ref in T]: Root[T] extends mongoose.Types.Array<infer U>
    ? mongoose.Types.Array<Exclude<U, mongoose.Types.ObjectId>>
    : Exclude<Root[T], mongoose.Types.ObjectId>;
};

/**
 * Populate properties on a document type:
 * ```
 * import { PopulatedDocument } from "../interfaces/mongoose.gen.ts"
 *
 * function example(user: PopulatedDocument<UserDocument, "bestFriend">) {
 *   console.log(user.bestFriend._id) // typescript knows this is populated
 * }
 * ```
 */
export type PopulatedDocument<DocType, T> = T extends keyof DocType
  ? PopulatedProperty<DocType, T>
  : ParentProperty<T> extends keyof DocType
  ? Omit<DocType, ParentProperty<T>> & {
      [ref in ParentProperty<T>]: DocType[ParentProperty<T>] extends mongoose.Types.Array<
        infer U
      >
        ? mongoose.Types.Array<
            ChildProperty<T> extends keyof U
              ? PopulatedProperty<U, ChildProperty<T>>
              : PopulatedDocument<U, ChildProperty<T>>
          >
        : ChildProperty<T> extends keyof DocType[ParentProperty<T>]
        ? PopulatedProperty<DocType[ParentProperty<T>], ChildProperty<T>>
        : PopulatedDocument<DocType[ParentProperty<T>], ChildProperty<T>>;
    }
  : DocType;

/**
 * Helper types used by the populate overloads
 */
type Unarray<T> = T extends Array<infer U> ? U : T;
type Modify<T, R> = Omit<T, keyof R> & R;

/**
 * Augment mongoose with Query.populate overloads
 */
declare module "mongoose" {
  interface Query<ResultType, DocType, THelpers = {}> {
    populate<T extends string>(
      path: T,
      select?: string | any,
      model?: string | Model<any, THelpers>,
      match?: any
    ): Query<
      ResultType extends Array<DocType>
        ? Array<PopulatedDocument<Unarray<ResultType>, T>>
        : ResultType extends DocType
        ? PopulatedDocument<Unarray<ResultType>, T>
        : ResultType,
      DocType,
      THelpers
    > &
      THelpers;

    populate<T extends string>(
      options: Modify<PopulateOptions, { path: T }> | Array<PopulateOptions>
    ): Query<
      ResultType extends Array<DocType>
        ? Array<PopulatedDocument<Unarray<ResultType>, T>>
        : ResultType extends DocType
        ? PopulatedDocument<Unarray<ResultType>, T>
        : ResultType,
      DocType,
      THelpers
    > &
      THelpers;
  }
}
